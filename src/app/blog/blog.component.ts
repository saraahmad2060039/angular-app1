import { Component } from '@angular/core';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css'],
})
export class BlogComponent {
  friends: string[] = ['ali', 'soha', 'toka', 'ahmed'];
  isActive: boolean = true;
  ay7aga: number = 12;
  str: string = 'test1';
}
