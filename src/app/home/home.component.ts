import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';

import { Subscription } from 'rxjs';
import { Product } from '../product';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  cancelRequest!: Subscription;
  products: Product[] = [];
  searchTerm: string = '';
  constructor(private _productsService: ProductsService) {}
  ngOnInit(): void {
    this.getProducts();
  }
  getProducts() {
    this._productsService.getProducts().subscribe({
      next: (data) => {
        if (data?.length) {
          this.products = data;
        }
        console.log(data);
      },
      error: () => {},
    });
  }
}
//http interceptor

// layer between req ===interceptor==> server ==interceptor===> res
