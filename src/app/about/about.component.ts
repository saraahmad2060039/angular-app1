import { Component } from '@angular/core';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
})
export class AboutComponent {
  string_1: string = 'abcd';
  age: number = 2;
  imgSource: string = './../../assets/images/images.jpg';
  active: boolean = false;
  inputField: string = '';

  constructor(private _productsService: ProductsService) {
    // _productsService.getPosts().subscribe({
    //   next: (data) => {
    //     this.posts = data.slice(0, 10);
    //   },
    // });
  }

  sayHello() {
    console.log('hello');
  }

  printInConsole(e: KeyboardEvent) {
    const inputField = e.target as HTMLInputElement;
    console.log(inputField.value);
  }
}
