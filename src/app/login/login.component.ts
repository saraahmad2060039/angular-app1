import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  showPassword: boolean = false;
  loginForm: FormGroup = new FormGroup({
    userName: new FormControl(null, [
      Validators.required,
      Validators.minLength(3),
      Validators.maxLength(20),
    ]),
    email: new FormControl(null, [Validators.required, Validators.email]),
    password: new FormControl(null, [
      Validators.required,
      Validators.pattern(/^[0-9]{3,5}[a-zA-Z]{3,5}$/),
    ]),
  });

  displayPassword() {
    this.showPassword = !this.showPassword;
  }
  submitLoginForm(form: FormGroup) {
    localStorage.setItem('authorized', 'true');
    console.log(form);
  }
}
