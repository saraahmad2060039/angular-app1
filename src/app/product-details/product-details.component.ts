import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../product';
import { LoaderService } from '../loader.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css'],
})
export class ProductDetailsComponent implements OnInit {
  productId: string = '';
  productInfo: Product | undefined;
  constructor(
    private productsService: ProductsService,
    private _route: ActivatedRoute
  ) {}
  ngOnInit(): void {
    this.productId = this._route.snapshot.params['prodId'];
    this.getProduct();
  }
  getProduct() {
    this.productsService.getProduct(this.productId).subscribe({
      next: (data) => {
        this.productInfo = data;
        console.log(data);
      },
      error: (err) => {
        console.log(err);
      },
    });
  }
}
