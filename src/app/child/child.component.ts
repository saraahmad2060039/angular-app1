import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css'],
})
export class ChildComponent implements OnInit, OnChanges, OnDestroy {
  @Input() valueFromParent: string = '';

  @Output() sendDataToParent: EventEmitter<string> = new EventEmitter();
  constructor() {
    console.log('hello from constructor');
  }
  sayHello() {
    // console.log('hello');
    this.sendDataToParent.emit('hi from child event emitter');
  }
  ngOnInit(): void {
    console.log('hello from ngOnInit');
  }
  ngOnDestroy(): void {
    console.log('Hello from ngDestroy');
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log('hello from changes');
  }
}
