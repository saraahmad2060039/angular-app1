import { Component } from '@angular/core';
import { LoaderService } from '../loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css'],
})
export class LoaderComponent {
  showLoader: boolean = false;

  constructor(private _loaderService: LoaderService) {
    this._loaderService.isLoading.subscribe(
      (value) => (this.showLoader = value)
    );
  }
}
