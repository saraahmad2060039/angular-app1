import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogComponent } from './blog/blog.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { WebComponent } from './contact/web/web.component';
import { MobileComponent } from './contact/mobile/mobile.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { authGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent, title: 'login' },
  { path: 'register', component: RegisterComponent, title: 'register' },

  { path: 'blog', component: BlogComponent, title: 'Blog' },
  {
    path: 'productDetails/:prodId',
    canActivate: [authGuard],
    component: ProductDetailsComponent,
    title: 'productDetails',
  },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home',
    canActivate: [authGuard],
    component: HomeComponent,
    title: 'Home',
  },
  {
    path: 'contact',
    component: ContactComponent,
    title: 'Contact',
    children: [
      { path: '', redirectTo: 'web', pathMatch: 'full' },
      { path: 'web', component: WebComponent, title: 'web' },
      { path: 'mobile', component: MobileComponent, title: 'mobile' },
    ],
  },
  { path: 'about', component: AboutComponent, title: 'About' },

  { path: '**', component: NotFoundComponent, title: 'NotFound' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
